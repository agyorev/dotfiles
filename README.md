# dotfiles

## Table of Contents

* [Setup](#setup)
    1. [Required software](#1-required-software)
    2. [Clone the repo](#2-clone-the-repo)
    3. [Create symlinks](#3-create-symlinks-to-the-configuration-files)
    4. [Complete plugin installation](#4-complete-plugin-installation)
        * [tmux](#tmux)
        * [vim](#vim)

## Setup

### 1. Required software

1. **git** (version control) Follow the instructions from the [official git website](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).

2. **fzf** (fuzzy finder) Install following the *Homebrew* instructions from their [official GitHub page](https://github.com/junegunn/fzf#using-homebrew-or-linuxbrew).

3. **tmux** (terminal multiplexing) Follow the instructions from [this website](https://hackernoon.com/a-gentle-introduction-to-tmux-8d784c404340).

    1. **tpm** (tmux plugin manager) Use the following instructions from their [GitHub profile](https://github.com/tmux-plugins/tpm).
    2. **tmux-colors-solarized** (tmux color theme) Install using tpm following the official instructions from [GitHub](https://github.com/seebi/tmux-colors-solarized).
    3. **tmux-yank** (copy-paste support) Install the tpm plugin using the official instructions from [GitHib](https://github.com/tmux-plugins/tmux-yank).

4. **zsh** (unix shell, zsh > bash) Use the following [instructions](https://github.com/robbyrussell/oh-my-zsh/wiki/Installing-ZSH).

    1. **oh-my-zsh** (zsh plugin manager) Follow the official instructions from [Oh My Zsh's GitHub](https://github.com/robbyrussell/oh-my-zsh/).
    2. **zsh-syntax-highlighting** (highlight commands while you type them) Follow the *oh-my-zsh* commands from the plugin's [official GitHub page](https://github.com/zsh-users/zsh-syntax-highlighting/blob/master/INSTALL.md).
    3. **zsh-autosuggestions** (suggesting commands as you type) Follow the *oh-my-zsh* instructions from the plugin's [official GitHub page](https://github.com/zsh-users/zsh-autosuggestions/blob/master/INSTALL.md).
    4. **powerlevel9k** (awesome zsh theme) Follow the *oh-my-zsh* instructions from their [official GitHub page](https://github.com/bhilburn/powerlevel9k/wiki/Install-Instructions#option-2-install-for-oh-my-zsh).

5. **neovim** (better supported vim) Follow the official instructions from [Neovim's GitHub](https://github.com/neovim/neovim/wiki/Installing-Neovim).

    1. **vim-plug** (plugin manager) Install using the **Neovim** command from the official [GitHub page](https://github.com/junegunn/vim-plug).

### 2. Clone the repo

```bash
~ $ git clone git@gitlab.com:agyorev/dotfiles.git
```

### 3. Create symlinks to the configuration files
```bash
ln -s ~/dotfiles/.bash_profile ~/.bash_profile
ln -s ~/dotfiles/.zshrc ~/.zshrc
ln -s ~/dotfiles/.tmux.conf ~/.tmux.conf
ln -s ~/dotfiles/init.vim ~/.config/nvim/init.vim
```

### 4. Complete plugin installation

#### tmux
Open a `tmux` session and do `Prefix + I`, in our case that is `<C-a> + I`.

#### vim
Open `nvim` and do `:PlugInstall`.