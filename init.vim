" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.local/share/nvim/plugged')

Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'scrooloose/nerdcommenter'
Plug 'itchyny/lightline.vim'
Plug 'terryma/vim-multiple-cursors'
Plug 'w0rp/ale'

" Requires to have fzf installed
Plug '/usr/local/opt/fzf'
Plug 'junegunn/fzf.vim'  

call plug#end()

let g:lightline = {
      \ 'colorscheme': 'solarized',
      \ }

if !has('gui_running')
  set t_Co=256
endif

" Map :Files to Ctrl+P
map <C-p> :Files<CR>

" Enable filetype plugins
filetype plugin on

" Enable auto-indentation
filetype plugin indent on

" Get rid of that dreadful 'Shift' key-pressing
nnoremap ; :

" Copy until end of line, similarly to C and D
nnoremap Y y$

" Set to auto read when a file is changed from the outside
set autoread

" Leader is the comma
let mapleader=","

" Fast saving
nmap <leader>w :w!<cr>

" Fast closing
nmap <leader>q :q<cr>

" Map jk to esc
inoremap jk <esc>

" Turn on the Wild menu
" Visual autocomplete for command menu
set wildmenu

" Ignore compiled files
set wildignore=*.o,*~,*.pyc

" Always show current position
set ruler

" Height of the command bar
set cmdheight=2

" Show command in bottom bar
set showcmd

" A buffer becomes hidden when it is abandoned
set hid

" Ignore case when searching
set ignorecase

" When searching try to be smart about cases
set smartcase

" Make search act like in modern browsers
set incsearch

" Highlight search results
set hlsearch

" Unhighlight search matches
nmap <silent> ,/ :nohlsearch<CR>

" Show relative line numbers
set relativenumber

" Don't redraw while executing macros (good performance config)
set lazyredraw

" For regular expressions turn magic on
set magic

" Show matching brackets when text indicator is over them
set showmatch
" How many tenths of a second to blink when matching brackets
set mat=2

" No annoying sound on errors
set noerrorbells
set novisualbell
set t_vb=
set tm=500

" Set utf8 as standard encoding and en_US as the standard language
set encoding=utf8

" Turn backup off
set nobackup
set nowb
set noswapfile

" Use spaces instead of tabs
set expandtab

" Be smart when using tabs
set smarttab

set autoindent "Auto indent

set wrap

" Number of visual spaces per tab
set tabstop=2

"Number of spaces in tab when editing
set softtabstop=2

set shiftwidth=2

" More natural split opening
set splitbelow
set splitright

" Open NERDTree
map <Leader>n :NERDTree<CR>

" Going old C style
set colorcolumn=101

" Highlighting the current line
set cursorline

set history=1000
set undolevels=1000

set hidden

" Disable arrow keys
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>

" Move vertically by visual line (e.g. when a line is wraped)
nnoremap j gj
nnoremap k gk

" Easier split navigations
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" Easier tab navigation
map <C-m> gt
map <C-n> gT

" Maps Shift-[h,j,k,l] to resizing a window split
map <silent> <S-h> <C-w><
map <silent> <S-j> <C-W>-
map <silent> <S-k> <C-W>+
map <silent> <S-l> <C-w>>

" Maps Control-[s.v] to horizontal and vertical split respectively
map <silent> <C-s> :split<CR>
map <silent> <C-a> :vsplit<CR>

" Useful mapping for managing tabs
map <leader>tn :tabnew<cr>
map <leader>to :tabonly<cr>
map <leader>tc :tabclose<cr>
map <leader>tm :tabmove

" Opens a new tab with the current buffer's path
" Super useful when editing files in the same dir
map <leader>te :tabedit <c-r>=expand("%:p:h")<cr>/

" Return to last edit position when opening files
" You want this!
autocmd BufReadPost *
    \ if line("'\"") > 0 && line("'\"") <= line("$") |
    \   exe "normal! g`\"" |
    \ endif
" Remember info about open buffers on close
set viminfo^=%

" Always show the status line
set laststatus=2
" Format the status line
set statusline=\ %{HasPaste()}%F%m%r%h\ %w\ \ CWD:\ %r%{getcwd()}%h\ \ \ Line:\ %l

" Remav VIM 0 to fist non-blank character
map 0 ^

" Delete trailing white space on save, useful for Python and CoffeeScript
func! DeleteTrailingWS()
    exe "normal mz"
    %s/\s\+$//ge
    exe "normal `z"
endfunc
autocmd BufWrite *.py :call DeleteTrailingWS()
autocmd BufWrite *.coffee :call DeleteTrailingWS()

" Highlist last inserted text
nnoremap gV `[`v`]

" Toggle paste mode on and off
map <leader>pp :setlocal paste!<cr>

" Returns true if paste mode is on
function! HasPaste()
    if &paste
        return 'PASTE MODE  '
    en
    return ''
endfunction

" Enable folding
set foldenable

" Open most folds by default
set foldlevelstart=10

" Ten nested fold max
set foldnestmax=10

" Space open/closes folds
nnoremap <space> za

" Fold based on indent level
set foldmethod=indent

" This is a slew of commands that create language-specific settings for certain
" filetypes/file extensions. It is important to note they are wrapped in an
" augroup as this ensures the autocmd's are only applied once. In addition, the
" autocmd! directive clears all the autocmd's for the current group.
"augroup configgroup
"    autocmd!
"    autocmd VimEnter * highlight clear SignColumn
"    "autocmd BufWritePre *.php,*.py,*.js,*.txt,*.hs,*.java,*.md
"                "\:call <SID>StripTrailingWhitespaces()
"    autocmd FileType java setlocal noexpandtab
"    autocmd FileType java setlocal list
"    autocmd FileType java setlocal listchars=tab:+\ ,eol:-
"    autocmd FileType java setlocal formatprg=par\ -w80\ -T4
"    autocmd FileType php setlocal expandtab
"    autocmd FileType php setlocal list
"    autocmd FileType php setlocal listchars=tab:+\ ,eol:-
"    autocmd FileType php setlocal formatprg=par\ -w80\ -T4
"    autocmd FileType ruby setlocal tabstop=2
"    autocmd FileType ruby setlocal shiftwidth=2
"    autocmd FileType ruby setlocal softtabstop=2
"    autocmd FileType ruby setlocal commentstring=#\ %s
"    autocmd FileType python setlocal commentstring=#\ %s
"    autocmd FileType python setlocal shiftwidth=2
"    autocmd BufEnter *.cls setlocal filetype=java
"    autocmd BufEnter *.zsh-theme setlocal filetype=zsh
"    autocmd BufEnter Makefile setlocal noexpandtab
"    autocmd BufEnter *.sh setlocal tabstop=2
"    autocmd BufEnter *.sh setlocal shiftwidth=2
"    autocmd BufEnter *.sh setlocal softtabstop=2
"augroup END

