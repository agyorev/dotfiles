A folder containing all of the necessary source files for oh-my-zsh, nvim and tmux setup and plugins.

This still requires you to have zsh, fzf, nvim and tmux installed on your system.

