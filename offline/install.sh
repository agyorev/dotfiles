#!/bin/sh
#
#  Copy the contents of the oh-my-zsh, nvim-plugins and tmux-plugins folders to their respective
#  places in order for them to be installed correctly.
#

set -x

DIRNAME=$(dirname "$0")

cp -rf ${DIRNAME}/oh-my-zsh/.oh-my-zsh ~/

mkdir -p ~/.local/share/nvim
cp -rf ${DIRNAME}/nvim-plugins/plugged ~/.local/share/nvim

mkdir -p ~/.tmux
cp -rf ${DIRNAME}/tmux-plugins/plugins ~/.tmux

mkdir -p ~/.local/share/nvim/site/autoload/
cp -rf ${DIRNAME}/nvim-plugin-manager/plug.vim ~/.local/share/nvim/site/autoload/
