#!/bin/sh
#
#  Update the contents of the oh-my-zsh, nvim-plugins and tmux-plugins folders.
#

set -x

DIRNAME=$(dirname "$0")

mkdir -p ${DIRNAME}/oh-my-zsh
cp -rf ~/.oh-my-zsh ${DIRNAME}/oh-my-zsh/

mkdir -p ${DIRNAME}/nvim-plugins
cp -rf ~/.local/share/nvim/plugged ${DIRNAME}/nvim-plugins/

mkdir -p ${DIRNAME}/tmux-plugins
cp -rf ~/.tmux/plugins ${DIRNAME}/tmux-plugins

mkdir -p ${DIRNAME}/nvim-plugin-manager
cp -rf ~/.local/share/nvim/site/autoload/plug.vim ${DIRNAME}/nvim-plugin-manager/

find ${DIRNAME}/. | grep "\.git$" | xargs rm -rf
find ${DIRNAME}/. | grep "\.github$" | xargs rm -rf
find ${DIRNAME}/. | grep "\.gitignore$" | xargs rm -rf
find ${DIRNAME}/. | grep "\.gitattributes$" | xargs rm -rf

